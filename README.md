# AI Agent for Pong

An AI agent for the game of Pong available on the OpenAI Gym. 
</br>
</br>
</br>


<p align="center"><img src="images/pong.gif" width="640" alt="Pong"></p>

<p align="center"><i>Pong AI using OpenAI Gym and PyTorch</i> - Watch the full game on <a href="https://youtu.be/C2D0pl2a-EQ">YouTube</a></p>

</br>

In the gameplay shown above, the left paddle is the default AI, and the right paddle is the custom AI trained by me.
The training process is simple. First Initilize the AI agent and teach it the basics using supervised learning. For this I created a dataset from the recordings of my own gameplay. With the help of supervised learning the agent learned to follow the ball. 
After this initialization, Reinforcement learning was used to fine tune the AI agent. It is during reinforcement learning that the Agent learns to exploit the game to win and earn rewards. 

In theory, it is not necessary to initialize the agent using supervised learning before reinforcement learning, but in practice it leads to much better performance and shorter train time.

OpenCV was used in this project to extract data from the input frame, while the agent is a deep dense network of structure: (24-64-64-32-3).

The AI Agent trained in this project wins the default AI with an average score of 21:10.77.
