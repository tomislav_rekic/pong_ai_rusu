import torch
import cv2
import numpy as np


def find_coordinates(image):
    image = np.array(image)
    image = cv2.dilate(image, np.ones(shape=(3, 3)))

    _, contours, _ = cv2.findContours(image.copy(), cv2.RETR_CCOMP, cv2.CHAIN_APPROX_TC89_L1)

    p1x, p1y, bx, by, p2x, p2y = 0, 0, 0, 0, 0, 0

    for i in range(len(contours)):
        M = cv2.moments(contours[i])

        if M["m00"] != 0:
            cX = int(M["m10"] / M["m00"])
            cY = int(M["m01"] / M["m00"])
        else:
            cX, cY = 0, 0

        if cX == 9:
            p1x = cX
            p1y = cY

        elif cX == 74:
            p2x = cX
            p2y = cY

        else:
            bx = cX
            by = cY

    if len(contours) < 3:
        bx = p2x
        by = p2y

    return torch.tensor((p1x, p1y, bx, by, p2x, p2y), dtype=torch.float32)