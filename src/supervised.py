import torch
import gym
import collections
import random
import cv2
import time
import screen_interpreter2 as inter
from pynput import keyboard
from datetime import datetime
from gym.envs.classic_control import rendering
import numpy as np


FRAME_HISTORY_SIZE = 4
MAX_NOOP_ITERS = 30
GAMMA = 0.9995
DATA_SAVE_DIR = "../training_data/complete/"
dont_move_threshold = 1

action = 0
go_up = False
go_down = False

use_cuda = torch.cuda.is_available()
device = torch.device('cuda') if use_cuda else torch.device('cpu')
viewer = rendering.SimpleImageViewer()


def repeat_upsample(rgb_array, k=1, l=1):
    return np.repeat(np.repeat(rgb_array, k, axis=0), l, axis=1)


def process_frame():
    screen = env.render(mode='rgb_array')
    upscaled = repeat_upsample(screen, 3, 3)
    viewer.imshow(upscaled)
    gray = cv2.cvtColor(screen, cv2.COLOR_BGR2GRAY)
    gray = cv2.resize(gray, (84, 110))[18:102, :]
    gray[gray == 64] = 0
    gray[gray == 111] = 0
    gray[gray != 0] = 255
    return torch.tensor(gray, dtype=torch.uint8, device=device)


def get_data_from_image(image):
    coordinates = inter.find_coordinates(image)
    #print(coordinates)
    return torch.tensor(coordinates, dtype=torch.float32, device=device)


def prep_state_for_model(state):
    return torch.tensor(state/255, dtype=torch.float32, device=device)

def savename_gen(success, date, length):
    return "pong_recording_{}_{}_len:{}".format("success" if success else "fail", date, length)


def on_press(key):
    global go_up,go_down
    if key == keyboard.Key.up:
        go_up = True
    elif key == keyboard.Key.down:
        go_down = True

def on_release(key):
    global go_up, go_down
    if key == keyboard.Key.up:
        go_up = False
    elif key == keyboard.Key.down:
        go_down = False

#listener = keyboard.Listener(on_press=on_press, on_release=on_release)
#listener.start()
env = gym.make("Pong-v4")
while True:
    env.reset()
    action_data = []
    screen_data = []
    done = False
    steps = 0
    while not done:
        steps += 1
        #time.sleep(1/35.)
        #env.render()
        coords = get_data_from_image(process_frame())
        action = 0
        diff = coords[3]-coords[5]
        if abs(diff) > dont_move_threshold:
            if coords[3] > coords[5]:
                action = 5
            elif coords[3] < coords[5]:
                action = 2

        print("ball: {}  player: {}  diff: {}  action: {}".format(coords[3], coords[5], diff, action))
        """
        if go_down and go_up:
            action = 0
        elif go_up:
            action = 2
        elif go_down:
            action = 5

        """

        action_data.append(action)
        screen_data.append(coords)

        _,reward, done,_= env.step(action)
        if reward != 0:
            filename = DATA_SAVE_DIR + savename_gen(True, datetime.now().strftime("%d-%m-%Y_%H:%M:%S"), steps)
            file = open(filename, "w+")
            for i in range(steps):
                tmp = screen_data[i]
                line = "{},{},{},{},{},{},{}\n".format(action_data[i],tmp[0],tmp[1],tmp[2],tmp[3],tmp[4],tmp[5])
                file.write(line)

            steps = 0
            action_data.clear()
            screen_data.clear()
            file.close()
            print("reward: " + str(reward))

