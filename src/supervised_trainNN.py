import torch
import torch.nn.functional as F
from models.nnINV import nn1
import collections
import random
import glob

SAVE_NAME = "../saves/saveSUPERNN_3.pt"
DATA_SAVE_DIR = "../training_data/complete/"
resume = True
allow_cuda = False
input_size = 84
learning_rate = 0.001
episodes = 1000000
FRAME_HISTORY_SIZE = 4
GAMMA = 0.99


actions = {
    0: 0,
    2: 1,
    5: 2
}


class FrameBuffer:
    def __init__(self, size):
        self.size = size
        self.buffer = collections.deque([], size)

    def push(self, frame):
        self.buffer.append(frame)

    def is_full(self):
        for item in self.buffer:
            if item is None:
                return False
        return True

    def get_frame_tensor(self):
        assert(self.is_full()), "Cant get frames, buffer is not full"
        return torch.stack(list(self.buffer)).unsqueeze(0)


def savename_gen(success, date, length):
    return "pong_recording_{}_{}_len:{}".format("success" if success else "fail", date, length)


def prep_state_for_model(state):
    return torch.tensor(state/255, dtype=torch.float32, device=device)


use_cuda = torch.cuda.is_available()
device = torch.device('cuda') if use_cuda and allow_cuda else torch.device('cpu')

if resume:
    try:
        model = torch.load(SAVE_NAME, map_location=torch.device('cpu'))
    except Exception:
        print("Creating new model")
        model = nn1(input_size)
else:
    print("Creating new model")
    model = nn1(input_size)

frame_buffer = FrameBuffer(FRAME_HISTORY_SIZE)

optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
optimizer.zero_grad()

model.to(device)

dataset = []
path = DATA_SAVE_DIR + savename_gen(True, "*","*")
filenames = glob.glob(path)
for k in range(len(filenames)):
    filename = filenames[k]
    filename_alt = filename.replace("len:", "")
    tokens = filename_alt.split("_")
    file_len = int(tokens[len(tokens)-1])
    file = open(filename, "r+")
    action_data = []
    screen_data = []
    for j in range(file_len):
        input_string = file.readline()
        input_string = input_string.split(",")
        action_data.append(int(input_string[0]))
        screen = []
        for z in range(1,7):
            screen.append(float(input_string[z]))
        screen_data.append(torch.tensor(screen, dtype=torch.float32, device=device))
    dataset.append((action_data,screen_data))


for i in range(episodes):
    next = random.randint(0,len(dataset)-1)
    next_actions, next_screens = dataset[next]
    count = len(next_actions)

    chances = []
    chances.append(next_actions.count(0) / count)
    chances.append(next_actions.count(2) / count)
    chances.append(next_actions.count(5) / count)

    loss_multiplier = []

    lossls = []
    for j in range(count):
        frame_buffer.push(next_screens[j])

        if j >= FRAME_HISTORY_SIZE:
            state = frame_buffer.get_frame_tensor()
            pred_y = model(prep_state_for_model(state))
            y = torch.zeros(size=(1,3))
            y[0, actions.get(next_actions[j])] = 1

            loss = F.smooth_l1_loss(pred_y, y).requires_grad_(True)
            lossls.append(loss)

            loss_multiplier.append(chances[actions.get(next_actions[j])])

    losst = torch.stack(lossls).requires_grad_(True).to(device)
    loss_multiplier_t = torch.tensor(loss_multiplier).to(device)

    #loss_multiplier_t -= torch.mean(loss_multiplier_t)
    #loss_multiplier_t /= torch.std(loss_multiplier_t)
    losst = (losst / loss_multiplier_t) * 0.1


    #curr_coef = 2
    #for h in reversed(range(len(losst))):
    #    losst[h] = losst[h] * curr_coef
    #    curr_coef = curr_coef * GAMMA


    grad_sum = torch.sum(losst)

    print("Episode: {}   Loss: {:.5} ".format(i, grad_sum.item()))

    optimizer.zero_grad()
    grad_sum.backward()
    optimizer.step()

    if i % 10 == 0: torch.save(model, SAVE_NAME)
