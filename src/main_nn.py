import torch
import torch.nn.functional as F
import gym
from models.nnINV import nn1
import collections
import random
import cv2
import time
from src import screen_interpreter2 as inter

FRAME_HISTORY_SIZE = 4
MAX_NOOP_ITERS = 30
GAMMA = 0.9995
SAVE_NAME = "saves/saveSUPERNN_3.pt"
SAVE_CHECKPOINTS = "saves/checkpoints/third/"
checkpoint_interval = 100
resume = True
render = True
realtime = True
print_act = False
allow_cuda = False
input_size = 84
learning_rate = 0.000001
frame_history_num = 4
max_noop_iters = 30
episodes = 10000
SAMPLER_SIZE = 5
reward_sum = 0
avg_reward = 0

use_cuda = torch.cuda.is_available()
device = torch.device('cuda') if use_cuda and allow_cuda else torch.device('cpu')

def process_frame():
    screen = env.render(mode='rgb_array')
    gray = cv2.cvtColor(screen, cv2.COLOR_BGR2GRAY)
    gray = cv2.resize(gray, (84, 110))[18:102, :]
    gray[gray == 64] = 0
    gray[gray == 111] = 0
    gray[gray != 0] = 255
    return torch.tensor(gray, dtype=torch.uint8, device=device)


def get_data_from_image(image):
    coordinates = inter.find_coordinates(image)

    #print(coordinates)

    return torch.tensor(coordinates, dtype=torch.float32, device=device)


def run_noop_iters():
    iters = random.randint(FRAME_HISTORY_SIZE, MAX_NOOP_ITERS)
    for i in range(iters):
        env.step(env.action_space.sample())
        curr_frame = process_frame()
        get_data_from_image(curr_frame)
        curr_frame = get_data_from_image(curr_frame)
        frame_buffer.push(curr_frame)


def action_sampler(size, data):
    samples = size
    sample_counter = [0, 0, 0]
    noequals = False
    max_index = 0
    pred_y = data
    while noequals == False:
        for k in range(samples):
            for i in range(len(sample_counter)):
                if pred_y[0][i] > torch.rand(size=(1,1),device=device):
                    sample_counter[i] += 1

        max_index = 0
        for j in range(len(sample_counter)):
            if sample_counter[j] > sample_counter[max_index]:
                max_index = j

        noequals = True
        for j in range(len(sample_counter)):
            if j != max_index:
                if sample_counter[max_index] == sample_counter[j]:
                    noequals = False
    return max_index


def prep_state_for_model(state):
    return torch.tensor(state/255, dtype=torch.float32, device=device)


def discount_rewards(r):
    """ take 1D float array of rewards and compute discounted reward """
    discounted_r = torch.zeros_like(r)
    running_add = 0
    for t in reversed(range(len(r))):
        if r[t] != 0: running_add = 0  # reset the sum, since this was a game boundary (pong specific!)
        running_add = running_add * GAMMA + r[t]
        discounted_r[t] = running_add
    return discounted_r


class FrameBuffer:
    def __init__(self, size):
        self.size = size
        self.buffer = collections.deque([], size)

    def push(self, frame):
        self.buffer.append(frame)

    def is_full(self):
        for item in self.buffer:
            if item is None:
                return False
        return True

    def get_frame_tensor(self):
        assert(self.is_full()), "Cant get frames, buffer is not full"
        return torch.stack(list(self.buffer)).unsqueeze(0)


actionControls = {
    "noop": 0,
    "up": 2,
    "down": 5
}
actions = {
    0: "noop",
    1: "up",
    2: "down"
}

if resume:
    try:
        model = torch.load(SAVE_NAME, map_location=torch.device('cpu'))
    except Exception:
        print("Creating new model")
        model = nn1(input_size)
else:
    print("Creating new model")
    model = nn1(input_size)


frame_buffer = FrameBuffer(FRAME_HISTORY_SIZE)

optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
optimizer.zero_grad()

env = gym.make("PongDeterministic-v4")
observation = env.reset()

model.to(device)

for i in range(episodes):
    #observation = env.reset()
    if render: env.render()
    run_noop_iters()

    state = frame_buffer.get_frame_tensor()
    done = False

    lossls = []
    rewardls = []

    #optimizer.zero_grad()
    score = 0
    while not done:
        if realtime: time.sleep(1/40.) #realtime

        if render: env.render()

        pred_y = model(prep_state_for_model(state))

        action_index = action_sampler(SAMPLER_SIZE, pred_y)
        action = actionControls[actions[action_index]]

        if print_act: print("action taken {} : chance = {:1.4f} : : pred_y = {:1.4f} | {:1.4f} | {:1.4f} ".format(actionControls[actions[action_index]], pred_y[0][action_index], pred_y[0,0], pred_y[0,1], pred_y[0,2]))

        y = torch.zeros_like(pred_y)
        y[0][action_index] = 1

        loss = F.smooth_l1_loss(pred_y, y).requires_grad_(True)
        lossls.append(loss)
        _, reward, done, _ = env.step(action)
        rewardls.append(reward)
        new_state = process_frame()
        get_data_from_image(new_state)
        new_state = get_data_from_image(new_state)
        frame_buffer.push(new_state)
        state = frame_buffer.get_frame_tensor()
        if reward != 0:
            score += reward

    reward_sum += score
    n = (i+1) % checkpoint_interval
    if n != 0:
        avg_reward = reward_sum/n
    else:
        avg_reward = reward_sum/checkpoint_interval
        print("save, reward {}".format(avg_reward))
        reward_sum = 0
        torch.save(model, SAVE_CHECKPOINTS + "model_{}_score_{:.2f}".format(i+1, avg_reward))

    env.reset()
    losst = torch.stack(lossls).requires_grad_(True).to(device)
    rewardt = torch.tensor(rewardls, device=device)
    d_rewardt = discount_rewards(rewardt)
    d_rewardt -= torch.mean(d_rewardt)
    d_rewardt /= torch.std(d_rewardt)
    grad = (losst * d_rewardt).requires_grad_(True)
    grad_sum = torch.sum(grad).requires_grad_(True)

    optimizer.zero_grad()
    grad_sum.backward()
    optimizer.step()
    print("Episode: {} Loss: {:.4f} Avg_Reward: {:.3f} Score: {:.1f}".format(i + 1, grad_sum.detach(), avg_reward, score))

    if i % 10 == 0: torch.save(model, SAVE_NAME)