import torch.nn as nn
import math


class nn1(nn.Module):
    def __init__(self, inputSize):
        super(nn1, self).__init__()

        input_size = 24
        output_size = 3
        self.fcSizes = (64, 64, 32)
        self.dropout_percents = (0.1, 0.15, 0.0)

        self.fcLayers = nn.ModuleList()
        self.dropoutLayers = nn.ModuleList()
        self.relus = nn.ModuleList()

        for i in range(len(self.fcSizes)):
            inDim = input_size if i == 0 else self.fcSizes[i-1]
            self.fcLayers.append(nn.Linear(inDim, self.fcSizes[i]))
            self.dropoutLayers.append(nn.Dropout(p=self.dropout_percents[i]))
            self.relus.append(nn.ReLU())

        self.output_layer = nn.Linear(self.fcSizes[len(self.fcSizes) - 1], output_size)
        self.softmax = nn.Softmax()

    def forward(self, x):
        out = x
        out = out.view(out.size()[0], -1)

        for i in range(len(self.fcSizes)):
            out = self.fcLayers[i](out)
            out = self.dropoutLayers[i](out)
            out = self.relus[i](out)

        out = self.output_layer(out)
        out = self.softmax(out)
        return out
